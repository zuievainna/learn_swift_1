import UIKit


var str:String = "Hello, playground"

//print(str)
//
//var favouriteAnimal = "Cat"
//print(favouriteAnimal)
//
//var friendName = "Tania"
//print(friendName)
//
//var myAge = 24
//print(myAge)
//
//var someNumber = 14.5948
//print(someNumber)
//
//var anotherNumber = 1233.5780387459382920
//print(anotherNumber)
//
//var justOneMoreNumber = 12.3
//print(justOneMoreNumber)
//
//var guessedAnswer:Bool = false
//print(guessedAnswer)
//
//var currentYear:Int = 2018
//print(currentYear)
//
//currentYear = 2019
//print(currentYear)
//
//guessedAnswer = true
//print(guessedAnswer)
//
//myAge = 25
//print(myAge)
//
//
//
//
//var a:Int = 2
//var b:Int = 1
//
//a = b
//str = "24"
//str = String(29)
//myAge = Int(26)
//
//
//
//var c:Float = 2.3
//var d:Double = 13.9
//var e:Bool = true
//
//print(Int(c))
//print(Int(d))
//print(Int(round(d)))
//
//var myNumberOfApples = 19
//
//var numberOfDaysBeforeNewYear:Double = 27.5
//print(Int(round(numberOfDaysBeforeNewYear)))
//
//
//print(a + b)
//print(a - b)
//print(a * b)
//print(a / b)
//
//let f = 10

//
//let a = 10
//let b = 1
//let c = 3
//
//if (a < 4 || b < 4) && c != 3 {
//
//    print("branch 1")
//}
//else if a < 8 {
//
//    print("branch 2")
//}
//else if !(a == 10 && b == 1) {
//
//    print("branch 3")
//}
//else {
//
//    print("nothing was true")
//}
//
//
//let myBirthYear = 1994
//let lionchikBirthYear = 1993
//let yuliasikBirthYear = 1992
//
//if myBirthYear < lionchikBirthYear && lionchikBirthYear == yuliasikBirthYear {
//    print("Lionchik and Yuliasik were born in the same year")
//}
//else if myBirthYear == lionchikBirthYear && myBirthYear > yuliasikBirthYear {
//    print("My year of birth is either greater than Yuliasiks and greater than Lionchiks")
//}
//else if yuliasikBirthYear < lionchikBirthYear {
//    print("Yuliasik is older than Lionchik")
//}
//else {
//    print ("None of the comparisons were right")
//}
//
//
//
//
//
//var someCharacter:Character = "c"
//
////if someCharacter == "a" {
////    print("is an A")
////}
//
//switch someCharacter {
//
//case "a":
//    print("is an A")
//
//case "b", "c": //comma is "or"
//    print("is a B or C")
//
//default:
//    print("some fallback")
//}
//
//
//
//
//var animal:String = "cat"
//
//switch animal {
//
//case "dog":
//    print("Bark")
//
//case "cat":
//    print("Meow")
//
//default:
//    print("Meow meow")
//}
//
//
//
//var cash:Int = 5000
//
//switch cash {
//
//case 50:
//    print("Not enough")
//
//case 500:
//    print("Well, that's better")
//
//case 5000:
//    print("Now we're talking")
//
//default:
//    print("Only 000+ zeros accepted")
//}
//




for index in 1...5 { //1...5 means how many times the code will run
    
    print(index)
}

for _ in 1...5 {
    
    print("hello")
}

for index in 1...5 {
    
    var sum = 0
    sum += index //same as sum = sum + index
    print(sum)
}





//var sum = 0
//
//for index in 1...5 {
//
//    sum += index
//}
//print(sum)





var buttons = 5

for button in 1...5 {
    print("Red button")
}





var apples = 5

for apple in 1...5 {
    apples += apple
    print("apples")
}





var counter = 5

while counter > 0 {
    
    print("Hello")
    counter -= 1
}


repeat {
    
    print("Hello2")
    counter = counter + 1
}
while counter < 7






var someNumber = 23

while someNumber < 25 {
    
    print("Inna")
    someNumber += 1
}

repeat {

    print("Inna2")
    someNumber += 1
}
while someNumber < 29






var numberOfCandy = 67

while numberOfCandy > 25 {
    
    print("I got a lot of candy")
    numberOfCandy -= 7
}

repeat {
    
    print("I got a few candy")
    numberOfCandy -= 7
}
while numberOfCandy > 25




var date = 10

while date > 5 {
    
    print("It's the first week of December")
    date -= 2
}

repeat {
    
    print("It's the first week of December")
    date -= 4
}
while date > 3





var snowballsForSnowman = 3

while snowballsForSnowman < 99 {
    
    print("I can build a few snowmen")
    snowballsForSnowman += 3
}

repeat {
    
    print("I can build a ton of snowmen")
    snowballsForSnowman += 3
}
 while snowballsForSnowman < 188





var crowOnTheTree = 35

while crowOnTheTree > 20 {
    
    print("There're this many crows")
    crowOnTheTree -= 5
}

repeat {
    
    print("There're this few crows")
    crowOnTheTree -= 5
}
while crowOnTheTree > 0







func makeABouquetOfFlowers() {
    
    let numberOfRose = 5
    let numberOfTulips = 7
    let numberOfDaisies = 9
    let bouquet = numberOfRose + numberOfTulips + numberOfDaisies
    
    print(bouquet)
}

makeABouquetOfFlowers()

makeABouquetOfFlowers()





func numberOfPeople() {
    
    let asian = 99
    let american = 53
    let european = 27
    let numberOfPeople = asian + american + european
    
    print(numberOfPeople)
}

numberOfPeople()

numberOfPeople()





func howManyPiecesOfClothAreInMyWardrobe() {
    
    let dress = 3
    let jacket = 4
    let pants = 2
    let shirt = 5
    let cloth = dress + jacket + pants + shirt
    
    print(cloth)
}

howManyPiecesOfClothAreInMyWardrobe()
howManyPiecesOfClothAreInMyWardrobe()





func addTwoNumbers(callLabel number1:Int, callLabel2 number2:Int) -> Int {
    
    return number1 + number2
}

let sum = addTwoNumbers(callLabel: 3, callLabel2: 5)
print(sum)





func multiplyTwoNumbers(firstNumber:Int, secondNumber:Int) -> Int {
    
    return firstNumber * secondNumber
}

let result1 = multiplyTwoNumbers(firstNumber: 3, secondNumber: 4)
print(result1)





func substractNumber(greaterNumber:Int, smallerNumber:Int) -> Int {
    
    return greaterNumber - smallerNumber
}

let substractionResult = substractNumber(greaterNumber: 59, smallerNumber: 26)
print(substractionResult)





func findQuotient(divident someNumber:Int, divider anotherNumber:Int) -> Int {
    
    return someNumber / anotherNumber
}

let quotient = findQuotient(divident: 55, divider: 5)
print(quotient)






func findFactorialFromNumber(from number:Int) -> Int {
    var result = 1
    for index in 1...number {
        result *= index
    }
    return result
}

let result11 = findFactorialFromNumber(from: 2)
print(result11)

let result12 = findFactorialFromNumber(from: 2)
print(result12)

print()
print()
print()
print()

func findFactorial(from number: Int) -> Int {
    if number == 1 {
        return 1
    } else {
        print("\(number) * findFactorial(from:\(number - 1))")
        return number * findFactorial(from: number - 1)
    }
}


let result13 = findFactorial(from: 10)
print(result13)






class BlogPost {
    
    var title = ""
    var author = ""
    var body = ""
    var numberOfComments = 0
    
    func addComment() {
        numberOfComments += 1
    }
}

//BlogPost()    initialized class - created an object of the class

let myBlogPost = BlogPost()
myBlogPost.title = "Hello, Playground"
myBlogPost.author = "Inna Zuieva"
myBlogPost.body = "Hello"
myBlogPost.addComment()
print(myBlogPost.numberOfComments)


let mySecondBlogPost = BlogPost()
mySecondBlogPost.title = "Goodbye, Playground"
mySecondBlogPost.author = "Leonid Kokhnovych"
mySecondBlogPost.body = "Hello"
mySecondBlogPost.addComment()
mySecondBlogPost.addComment()
mySecondBlogPost.addComment()
print(mySecondBlogPost.numberOfComments)





class Plant {
    
    var color = ""
    var height = ""
    var season = ""
    var sizeOfFlowers = ""
    var fruits = Bool()
    var lifeLength = Int()
    var numberOfPlant = 0
    
    func plantAPlant() {
        numberOfPlant += 1
    }
}

// Plant()  - to initialize the object of the class

let rose = Plant()
rose.color = "Red"
rose.height = "Medium"
rose.season = "Summer"
rose.sizeOfFlowers = "Medium"
rose.fruits = false
rose.lifeLength = 1

rose.plantAPlant()
print(rose.numberOfPlant)


let magnolia = Plant()
magnolia.color = "Green"
magnolia.height = "High"
magnolia.season = "Spring"
magnolia.sizeOfFlowers = "Large"
magnolia.fruits = false
magnolia.lifeLength = 12

magnolia.plantAPlant()
magnolia.plantAPlant()
print(magnolia.numberOfPlant)





class Bird {
    
    var size = ""
    var vocal = Bool()
    var location = ""
    var flyingAbility = Bool()
    var numberOfBirds = 0
    
    func addABird() {
        numberOfBirds += 1
    }
}

let penguin = Bird()
penguin.size = "Medium"
penguin.vocal = false
penguin.location = "North"
penguin.flyingAbility = false
penguin.addABird()
print(penguin.numberOfBirds)


let hummingbird = Bird()
hummingbird.size = "Small"
hummingbird.vocal = false
hummingbird.location = "South"
hummingbird.flyingAbility = true
hummingbird.addABird()
hummingbird.addABird()
print(hummingbird.numberOfBirds)






class Car {
    
    var topSpeed = 200
    func drive() {
        print("Driving at \(topSpeed)")
    }
}

class Futurecar : Car {
    
    override func drive() {
    
        super.drive()
        print("and rockets boosting at 50")
    }
    func fly() {
        print("Flying")
    }
}

let myRide = Car()
myRide.topSpeed
myRide.drive()


let myNewRide = Futurecar()
myNewRide.topSpeed
myNewRide.drive()
myNewRide.fly()

print()
print()
print()
print()


class Weather {
    
    var temperature = Int()
    var windSpeed = Int()
    var skyCondition = ""
    
    func windIsBlowing() {
        print("Wind is blowing with the speed of \(windSpeed) miles per hour")
    }
}

class NastyWeather : Weather {
    
    override func windIsBlowing() {
        super.windIsBlowing()
        print("and visibility is extremely low")
    }
    func doNotGoOutside() {
        print("It's reccommended to stay indoors")
    }
}

let today = Weather()
today.temperature = 23
today.windSpeed = 50
today.skyCondition = "Blue"
today.windIsBlowing()

print()

let tomorrow = NastyWeather()
tomorrow.temperature = 13
tomorrow.windSpeed = 150
tomorrow.skyCondition = "Grey"
tomorrow.windIsBlowing()
tomorrow.doNotGoOutside()






class Person {
    
    var name = ""
    var age = 0
    
    init() {
        
    }
    
    init(_ name: String, _ age: Int) {
        self.name = name
        self.age = age
    }
}

var somebody = Person()
somebody.age
somebody.name

var somebodyElse = Person("Inna", 24)
somebodyElse.age
somebodyElse.name






class Location {
    var street = ""
    var building = 0
    
    init() {
        
    }
    
    init(on street: String, at building: Int) {
        self.street = street
        self.building = building
    }
}

var myAddress = Location()
myAddress.street
myAddress.building

var myOtherAddress = Location(on: "Halifax", at: 5166)
myOtherAddress.street
myOtherAddress.building






class somePerson {
    var name = ""
}

class SomeBlogPost {
    var title: String?
    var author: Person?
    var body = "Hey"
    var numberOfComments = 0
}

let post = SomeBlogPost()
print(post.body + " hello")

//Optional Binding
post.title = "Yo"

if let actualTitle = post.title {
    print(actualTitle + " salut")
}

// In case there's some value for 100% - Force unwrap
print(post.title! + " salut")

//Testing for nil
if post.title != nil {
    print(post.title! + " salut")
}

//Reversed check - if it is nil
if post.title == nil {
    //Optional contains no value
}



class Movie {
    var name: String?
    var year = 0
    var starring = "Johnny Depp"
    var budget = 0
}

let newMovie = Movie()
print(newMovie.starring + " the Superstar")

//optional binding

newMovie.name = "Superhero"

if let actualName = newMovie.name {
    print(actualName + " movie")
}

//force unwrap

print(newMovie.name! + " movie")

//checking if it's not nil

if newMovie.name != nil {
    print(newMovie.name! + " movie")
}

//checking if it's nil

if newMovie.name == nil {
    //do nothing
}
